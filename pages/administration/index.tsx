import Skeleton from "react-loading-skeleton";
import { signIn, signOut, useSession } from "next-auth/client";

import Nav from "@/components/nav";
import Container from "@/components/container";
import Entries from "@/components/entries";

import { useEntries } from "@/lib/swr-hooks";

export default function AdminPage() {
  const { entries, isLoading } = useEntries();
  const [session, loading] = useSession();
  if (isLoading) {
    return (
      <div>
        <Nav />
        <Container>
          <Skeleton width={180} height={24} />
          <Skeleton height={48} />
          <div className="my-4" />
          <Skeleton width={180} height={24} />
          <Skeleton height={48} />
          <div className="my-4" />
          <Skeleton width={180} height={24} />
          <Skeleton height={48} />
        </Container>
      </div>
    );
  }

  return (
    <>
      {session ? (
        <div>
          <Nav />
          Signed in as {session.user.name} <br />
          <button onClick={() => signOut()}>Sign out</button>
          <Container>
            <Entries entries={entries} />
          </Container>
        </div>
      ) : (
        <p>
          <p>You are not permitted to see this page.</p>
          <button onClick={() => signIn()}>Sign in</button>
        </p>
      )}
    </>
  );
}
