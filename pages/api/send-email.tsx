const sgMail = require('@sendgrid/mail')

export default async function(req, res) {
  sgMail.setApiKey(process.env.SENDGRID_API_KEY)

  console.log('process.env.SENDGRID_API_KEY', process.env.SENDGRID_API_KEY)
  const { email, name, phone } = req.body

  const content = {
    to: 'mega.energy@hotmail.com',
    from: 'no-reply@megaenergy.gr',
    subject: `New Message From - ${email}`,
    text: `name: ${name}, phone: ${phone}`,
    html: `<p>name: ${name},<br/> phone: ${phone},<br/> email: ${email}</p>`
  }

  try {
    await sgMail.send(content)
    res.status(200).send('Message sent successfully.')
  } catch (error) {
    console.log('ERROR', error)
    res.status(400).send('Mwssage not sent')
  }
}
