import { NextApiHandler } from 'next'
import Filter from 'bad-words'
import { query } from '../../lib/db'

const filter = new Filter()

const handler: NextApiHandler = async (req, res) => {
   
  const { id,name, phone,email, contacted } = req.body
  try {
    if (!id || !name || !email || !phone) {
      return res
        .status(400)
        .json({ message: '`id`, `Name`, `Phone` and `Email` are required' })
    }

    const results = await query(
      `
      UPDATE subscribers
      SET name = ?, email = ?, phone = ?, contacted = ?
      WHERE id = ?
      `,
      [filter.clean(name), filter.clean(email), filter.clean(phone),filter.clean(contacted), id]
    )

    return res.json(results)
  } catch (e) {
    res.status(500).json({ message: e.message })
  }
}

export default handler
