import { NextApiHandler } from "next";
import Filter from "bad-words";
import { query } from "../../lib/db";

const filter = new Filter();

const handler: NextApiHandler = async (req, res) => {
  const { name, phone, email } = req.body;
  try {
    if (!name || !email || !phone) {
      return res
        .status(400)
        .json({ message: "`Name` and `Phone` and `Email` are required" });
    }

    const results = await query(
      `
      INSERT INTO subscribers (name,
        phone,
        email,
        contacted)
      VALUES (?, ?, ?, 0)
      `,
      [name, phone, email]
      // [filter.clean(name), filter.clean(phone),  filter.clean(email)]
    );

    return res.json(results);
  } catch (e) {
    res.status(500).json({ message: e.message });
  }
};

export default handler;
