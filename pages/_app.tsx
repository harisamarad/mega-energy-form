import React from "react";
import App from "next/app";
import Head from "next/head";
import { Provider } from "next-auth/client";
 
import "@fortawesome/fontawesome-free/css/all.min.css";
import "assets/styles/tailwind.css";
export default class MyApp extends App {
  render() {
    const { Component, pageProps } = this.props;

    return (
      <React.Fragment>
        <Provider session={pageProps.session}>
          <Head>
            <meta
              name="viewport"
              content="width=device-width, initial-scale=1, shrink-to-fit=no"
            />
            <title>Mega Energy</title>
          </Head>

          <Component {...pageProps} /> 
        </Provider>
      </React.Fragment>
    );
  }
}
{
  /* <Layout>
</Layout> */
}
