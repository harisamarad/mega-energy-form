import MainNav from "@/components/main-nav";
import EntryForm from "@/components/entry-form";
import Footer from "@/components/Footers/Footer.js";
import Image from "next/image";
import HomeTabs from "@/components/tabs/homeTabs"
import ProfTabs from "@/components/tabs/profTabs"

// import heroImg from "/static/heron-home-top.png";
export default function IndexPage() {
  return (
    <div>
      <MainNav />
      <main>
        <section className="pb-20 mt-24 mb-12 relative block bg-gray-900">
          <Image
            alt="..."
            src="/static/iron.jpg"
            className="w-full align-middle rounded-lg"
            layout="fill"
          />{" "}
          <div
            className="bottom-auto top-0 left-0 right-0 w-full absolute pointer-events-none overflow-hidden -mt-20 h-20"
            style={{
              transform: "translateZ(0)",
              zIndex: 1111,
            }}
          >
            <svg
              className="absolute bottom-0 overflow-hidden"
              xmlns="http://www.w3.org/2000/svg"
              preserveAspectRatio="none"
              version="1.1"
              viewBox="0 0 2560 100"
              x="0"
              y="0"
            >
              <polygon
                className="text-gray-900 fill-current"
                points="2560 0 2560 100 0 100"
              ></polygon>
            </svg>
          </div>
          <div
            className="container mx-auto mt-24 mb-12 px-4 lg:pt-24 lg:pb-64"
            style={{
              zIndex: 1111,
              position: "relative",
            }}
          >
            <div
              className="flex flex-wrap text-center justify-center"
              style={{ backgroundColor: "#04333333" }}
            >
              <div className="w-full lg:w-8/12 px-4">
                <h2 className="text-4xl font-semibold text-white">
                  Έλα στον Ήρωνα
                </h2>
                <h3 className="text-2xl m-4 font-semibold text-gray-300 text-justify ">
                  Με Έκπτωση συνέπειας 45% και τη ΝΕΑ ασφαλή ανέπαφη υπογραφή
                  συμβολαίου
                </h3>

                <p className="text-lg leading-relaxed mt-4 mb-4 text-gray-300 text-justify ">
                  Με τη χαμηλότερη τιμή της αγοράς και χρέωση χαμηλότερη της
                  Νυχτερινής, για όλο το 24ωρο από μόλις 0,04317€*/kWh με
                  έκπτωση συνέπειας 45%.
                </p>
                <p className="text-lg leading-relaxed mt-4 mb-4 text-gray-300">
                  *Η χρέωση αφορά νυχτερινή κατανάλωση
                </p>
              </div>
            </div>
          </div>
          <section className="relative block mt-12 py-24 lg:pt-0  ">
            <div className="container mx-auto px-4">
              <div className="flex flex-wrap justify-center lg:-mt-64 -mt-48">
                <div className="  lg:w-8/12 px-4">
                  <div className="relative flex flex-col min-w-0 break-words   mb-6 shadow-lg rounded-lg bg-gray-300">
                    <div className="flex-auto p-5 lg:p-10">
                      <h4 className="text-2xl font-semibold">Έλα στον Ήρωνα</h4>
                      <h4 className="text-2xl font-semibold">
                        με Έκπτωση συνέπειας 45%
                      </h4>
                      <p className="leading-relaxed mt-1 mb-4 text-gray-600">
                        Συμπλήρωσε τη φόρμα Επικοινωνίας και θα επικοινωνήσουμε
                        άμεσα μαζί σου
                      </p>
                      <EntryForm />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </section>

        <section className="pb-20 bg-gray-300 -mt-24">
          <div className="container mx-auto px-4">
            <div className="flex flex-wrap">
              <div className="lg:pt-12 pt-6 w-full md:w-4/12 px-4 text-center">
                <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
                  <div className="px-4 py-5 flex-auto">
                    <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-red-400">
                      <i className="fas fa-project-diagram"></i>
                    </div>
                    <h6 className="text-xl font-semibold">Δίκτυο Συνεργατών</h6>
                    <p className="mt-2 mb-4 text-gray-600 text-justify">
                      Έμπειρα στελέχη από όλη την Ελλάδα, έτοιμα να σας καλύψουν
                      με ποιοτικές υπηρεσίες υψηλού επιπέδου και ασυναγώνιστες
                      τιμές
                    </p>
                  </div>
                </div>
              </div>

              <div className="w-full md:w-4/12 px-4 text-center">
                <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
                  <div className="px-4 py-5 flex-auto">
                    <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-blue-400">
                      <i className="fas fa-compress-arrows-alt"></i>
                    </div>
                    <h6 className="text-xl font-semibold">
                      Προσέγγιση των αναγκών σας
                    </h6>
                    <p className="mt-2 mb-4 text-gray-600 text-justify">
                      Η ομάδα μας πριν από οποιαδήποτε πρόταση, μελετά
                      προσεκτικά τις πραγματικές ανάγκες που έχετε σήμερα και
                      την πιθανότητα να διαφοροποιηθούν στο μέλλον.
                      <br /> Στη συνέχεια σας προτείνουμε τις διαθέσιμες λύσεις
                      οι οποίες είναι προσαρμοσμένες στις παρούσες και
                      μελλοντικές ανάγκες σας.
                    </p>
                  </div>
                </div>
              </div>

              <div className="pt-6 w-full md:w-4/12 px-4 text-center">
                <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
                  <div className="px-4 py-5 flex-auto">
                    <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-green-400">
                      <i className="fas fa-handshake"></i>
                    </div>
                    <h6 className="text-xl font-semibold">Πάντα δίπλα σας</h6>
                    <p className="mt-2 mb-4 text-gray-600 text-justify">
                      Η εξυπηρέτηση μας δε σταματά ποτέ.
                      <br /> Είμαστε στη διάθεση σας για τα ζητήματα που πιθανώς
                      να αντιμετωπίσετε και την κάλυψη των μελλοντικών σας
                      αναγκών.
                    </p>
                  </div>
                </div>
              </div>
            </div>

            <div id="for-home" className="flex flex-wrap items-center mt-32">
              <div className="w-full md:w-8/12 mr-auto ml-auto">
                <div className="text-gray-600 p-3 text-center inline-flex items-center justify-center w-16 h-16 mb-6 shadow-lg rounded-full bg-gray-100">
                  <i className="fas fa-home text-xl"></i>
                </div>
                <h3 className="text-3xl mb-2 font-semibold leading-normal">
                  Για το σπίτι
                </h3>
                <p className="text-lg font-light leading-relaxed mt-4 mb-4 text-gray-600">
                  Η χαμηλότερη τιμή της αγοράς
                </p>
                <HomeTabs></HomeTabs>
              </div>

              {/* <div className="w-full h-500-px md:w-4/12 px-4 mr-auto ml-auto">
                <div
                  style={{ height: "240px" }}
                  className="relative flex flex-col min-w-0 break-words bg-white w-full h-500-px mb-6 shadow-lg rounded-lg bg-gray-800"
                >
                  <Image
                    alt="oikiaka programmata"
                    src="/static/heron-home-2.jpg"
                    className="w-full h-500-px align-middle rounded-lg"
                    width="auto"
                    height="240"
                  />
                </div>
              </div> */}
            </div>
          </div>
        </section>

        <section className="relative py-20">
          <div
            className="bottom-auto top-0 left-0 right-0 w-full absolute pointer-events-none overflow-hidden -mt-20 h-20"
            style={{ transform: "translateZ(0)" }}
          >
            <svg
              className="absolute bottom-0 overflow-hidden"
              xmlns="http://www.w3.org/2000/svg"
              preserveAspectRatio="none"
              version="1.1"
              viewBox="0 0 2560 100"
              x="0"
              y="0"
            >
              <polygon
                className="text-white fill-current"
                points="2560 0 2560 100 0 100"
              ></polygon>
            </svg>
          </div>

          <div id="for-enterprise" className="container mx-auto  ">
            <div className="items-center  md:w-3/12  flex flex-wrap">
              {/* <div
                style={{ height: "240px" }}
                className="relative flex flex-col min-w-0 break-words bg-white h-500-px mb-6 shadow-lg rounded-lg bg-gray-800"
              >
                <Image
                  alt="epaggelmatika programmata"
                  className="max-w-full h-500-px rounded-lg shadow-lg"
                  src="/static/business-img.jpg"
                  width="360"
                  height="240"
                /> 
              </div> */}
              <div className="w-full md:w-8/12 ml-auto mr-auto px-4">
                <div className="md:pr-12">
                  <div className="text-gray-600 p-3 text-center inline-flex items-center justify-center w-16 h-16 mb-6 shadow-lg rounded-full bg-gray-300">
                    <i className="far fa-building text-xl"></i>
                  </div>
                  <h3 className="text-3xl font-semibold">Για Επιχειρήσεις</h3>
                  <p className="mt-4 mb-4 text-lg leading-relaxed text-gray-800 ">
                    Η χαμηλότερη τιμή της αγοράς
                  </p>
                  <ProfTabs />
                </div>
              </div>
            </div>
          </div>
        </section>

        <section
          id="contact-form-interest"
          className="relative block py-24 pt-4 bg-gray-900"
        >
          <section className="pb-20 relative block bg-gray-900">
            <div className="flex flex-wrap mt-12 justify-center">
              <div className="w-full lg:w-3/12 px-4 text-center">
                <div className="text-gray-900 p-3 w-12 h-12 shadow-lg rounded-full bg-white inline-flex items-center justify-center">
                  <i className="fas fa-medal text-xl"></i>
                </div>
                <h6 className="text-xl mt-5 font-semibold text-white">
                  45% Έκπτωση Συνέπειας
                </h6>
              </div>
              <div className="w-full lg:w-3/12 px-4 text-center">
                <div className="text-gray-900 p-3 w-12 h-12 shadow-lg rounded-full bg-white inline-flex items-center justify-center">
                  <i className="fas fa-paper-plane text-xl"></i>
                </div>
                <h5 className="text-xl mt-5 font-semibold text-white">
                  Χωρίς Δέσμευση Συμβολαίου
                </h5>
              </div>
              <div className="w-full lg:w-3/12 px-4 text-center">
                <div className="text-gray-900 p-3 w-12 h-12 shadow-lg rounded-full bg-white inline-flex items-center justify-center">
                  <i className="fas fa-hand-holding-usd text-xl"></i>
                </div>
                <h5 className="text-xl mt-5 font-semibold text-white">
                  Δώρο Παραμονής Πελάτη 20€
                </h5>
              </div>
            </div>
          </section>
          <div
            className="container mx-auto mt-12 mt-32 px-4"
            style={{ marginTop: "12rem" }}
          >
            <div className="flex flex-wrap justify-center lg:-mt-64 -mt-48">
              <div className="w-full lg:w-6/12 px-4">
                <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-gray-300">
                  <div className="flex-auto p-5 lg:p-10">
                    <h4 className="text-2xl font-semibold">Έλα στον Ήρωνα</h4>
                    <h4 className="text-2xl font-semibold">
                      με Έκπτωση συνέπειας 45%
                    </h4>
                    <p className="leading-relaxed mt-1 mb-4 text-gray-600">
                      Συμπλήρωσε τη φόρμα Επικοινωνίας και θα επικοινωνήσουμε
                      άμεσα μαζί σου
                    </p>
                    <EntryForm />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
      <Footer />
    </div>
  );
}
// Mega.energy@hotmail.com
