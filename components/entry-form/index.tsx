import { useState, useEffect } from "react";
import Router from "next/router";
import fetch from 'isomorphic-unfetch';

import Button from "@/components/button";

export default function EntryForm() {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [submitting, setSubmitting] = useState(false);
  const [disabled, setDisabled] = useState(true);
  const [message, setMessage] = useState('');

  useEffect(() => {
    if (!name || !email || !phone) {
      return;
    } else {
      setDisabled(false);
    }
  }, [name, email, phone]);
  async function submitHandler(e) {
    setSubmitting(true);
    e.preventDefault();
    try {
      const res = await fetch("/api/create-entry", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          name,
          email,
          phone,
        }),
      });

      const json = await res.json();
      if (!res.ok) throw Error(json.message);
      setSubmitting(false);
      handleSendMailOnSubmit(e);
      // console.log( res, "respose text")
    } catch (e) {
      throw Error(e.message);
    }
  }
  // const handleSendMailOnSubmit = async (e) => {
  //   e.preventDefault();
  //   const res = await fetch("/api/send-email", {
  //     method: "POST",
  //     headers: {
  //       "Content-Type": "application/json",
  //     },
  //     body: JSON.stringify({ name: name, email: email, phone: phone }),
  //   });
  //   const text = await res.text();
  //   //  console.log('text', text)
  // };
  const handleSendMailOnSubmit = async (e) => {
    e.preventDefault();

    // let merge_vars = [{"FNAME": name , "PHONE":phone}]
    // 3. Send a request to our API with the user's email address.
    const res = await fetch('/api/subscribe', {
      body: JSON.stringify({
        email: email,
          name: name,
          phone: phone
      }),
      headers: {
        'Content-Type': 'application/json'
      },
      method: 'POST'
    });
console.log('Iam Here')
    const { error } = await res.json();

    if (error) {
      // 4. If there was an error, update the message in state.
      setMessage(error);

      return;}
      setMessage('Success! 🎉 Within moments one of our representatives will contact you.');

  }

  return (
    <form onSubmit={submitHandler}>
      <div className="relative w-full mb-3 mt-8">
        <label
          htmlFor="name"
          className="block uppercase text-gray-700 text-xs font-bold mb-2"
        >
          <h3 className="font-bold">Ονοματεπώνυμο</h3>
        </label>
        <input
          id="name"
          className="px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:shadow-outline w-full ease-linear transition-all duration-150"
          type="text"
          name="name"
          placeholder="Γιάννης Καραγιάννης"
          value={name}
          onChange={(e) => setName(e.target.value)}
          required={true}
        />
      </div>
      <div className="relative w-full mb-3 mt-8">
        <label
          htmlFor="phone"
          className="block uppercase text-gray-700 text-xs font-bold mb-2"
        >
          <h3 className="font-bold">Τηλέφωνο</h3>
        </label>
        <input
          id="phone"
          className="px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:shadow-outline w-full ease-linear transition-all duration-150"
          type="tel"
          name="phone"
          placeholder="6912345678"
          pattern="[0-9]{10}"
          value={phone}
          onChange={(e) => setPhone(e.target.value)}
          required={true}
          size={10}
          minLength={10}
          maxLength={10}
        />
        <small>Format: 6912345678</small>
      </div>
      <div className="relative w-full mb-3 mt-8">
        <label
          htmlFor="email"
          className="block uppercase text-gray-700 text-xs font-bold mb-2"
        >
          <h3 className="font-bold">Email</h3>
        </label>
        <input
          id="email"
          className="px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:shadow-outline w-full ease-linear transition-all duration-150"
          type="email"
          name="email"
          placeholder="john@doe.gr"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          required={true}
        />
      </div>
      <Button disabled={disabled ? true : false} type="submit">
        {submitting ? "ΤΟ ΑΙΤΗΜΑ ΣΑΣ ΑΠΕΣΤΑΛΕΙ" : "θελω προσφορα"}
      </Button>
    </form>
  );
}
