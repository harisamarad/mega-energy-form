import React from "react";

const HomeTabs = () => {
  const [openTab, setOpenTab] = React.useState(1);
  return (
    <>
      <div className="flex flex-wrap">
        <div className="w-full">
          <ul
            className="flex mb-0 list-none flex-wrap pt-3 pb-4 flex-row"
            role="tablist"
          >
            <li className="-mb-px mr-2 last:mr-0 flex-auto text-center">
              <a
                className={
                  "text-xs font-bold uppercase px-3 py-3 shadow-lg rounded block leading-normal " +
                  (openTab === 1
                    ? "text-white bg-gray-800"
                    : "text-gray-600 bg-white")
                }
                onClick={(e) => {
                  e.preventDefault();
                  setOpenTab(1);
                }}
                data-toggle="tab"
                href="#link1"
                role="tablist"
              >
                UP
              </a>
            </li>
            <li className="-mb-px mr-2 last:mr-0 flex-auto text-center">
              <a
                className={
                  "text-xs font-bold uppercase px-3 py-3 shadow-lg rounded block leading-normal " +
                  (openTab === 2
                    ? "text-white bg-gray-800"
                    : "text-gray-600 bg-white")
                }
                onClick={(e) => {
                  e.preventDefault();
                  setOpenTab(2);
                }}
                data-toggle="tab"
                href="#link2"
                role="tablist"
              >
                ΝΥΧΤΕΡΙΝΗ ΧΡΕΩΣΗ 24/7
              </a>
            </li>
            <li className="-mb-px mr-2 last:mr-0 flex-auto text-center">
              <a
                className={
                  "text-xs font-bold uppercase px-3 py-3 shadow-lg rounded block leading-normal " +
                  (openTab === 3
                    ? "text-white bg-gray-800"
                    : "text-gray-600 bg-white")
                }
                onClick={(e) => {
                  e.preventDefault();
                  setOpenTab(3);
                }}
                data-toggle="tab"
                href="#link3"
                role="tablist"
              >
                HOME 1
              </a>
            </li>
            <li className="-mb-px mr-2 last:mr-0 flex-auto text-center">
              <a
                className={
                  "text-xs font-bold uppercase px-3 py-3 shadow-lg rounded block leading-normal " +
                  (openTab === 4
                    ? "text-white bg-gray-800"
                    : "text-gray-600 bg-white")
                }
                onClick={(e) => {
                  e.preventDefault();
                  setOpenTab(4);
                }}
                data-toggle="tab"
                href="#link4"
                role="tablist"
              >
                ΗΟΜΕ 1Ν
              </a>
            </li>
            <li className="-mb-px mr-2 last:mr-0 flex-auto text-center">
              <a
                className={
                  "text-xs font-bold uppercase px-3 py-3 shadow-lg rounded block leading-normal " +
                  (openTab === 5
                    ? "text-white bg-gray-800"
                    : "text-gray-600 bg-white")
                }
                onClick={(e) => {
                  e.preventDefault();
                  setOpenTab(5);
                }}
                data-toggle="tab"
                href="#link5"
                role="tablist"
              >
                DOUBLE FIX ECONOMY
              </a>
            </li>
            <li className="-mb-px mr-2 last:mr-0 flex-auto text-center">
              <a
                className={
                  "text-xs font-bold uppercase px-3 py-3 shadow-lg rounded block leading-normal " +
                  (openTab === 6
                    ? "text-white bg-gray-800"
                    : "text-gray-600 bg-white")
                }
                onClick={(e) => {
                  e.preventDefault();
                  setOpenTab(6);
                }}
                data-toggle="tab"
                href="#link6"
                role="tablist"
              >
                DOUBLE FLEX ECONOMY
              </a>
            </li>
            <li className="-mb-px mr-2 last:mr-0 flex-auto text-center">
              <a
                className={
                  "text-xs font-bold uppercase px-3 py-3 shadow-lg rounded block leading-normal " +
                  (openTab === 7
                    ? "text-white bg-gray-800"
                    : "text-gray-600 bg-white")
                }
                onClick={(e) => {
                  e.preventDefault();
                  setOpenTab(7);
                }}
                data-toggle="tab"
                href="#link7"
                role="tablist"
              >
                EcoDrive HOME & HOME CHARGE
              </a>
            </li>
          </ul>
          <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-lg rounded">
            <div className="px-4 py-5 flex-auto">
              <div className="tab-content tab-space">
                <div className={openTab === 1 ? "block" : "hidden"} id="link1">
                  <h3>
                    <strong>ΧΑΡΑΚΤΗΡΙΣΤΙΚΑ</strong>
                  </h3>
                  <p>
                  Ο ΗΡΩΝ, ο κορυφαίος ιδιώτης παραγωγός και προμηθευτής
                    Ηλεκτρικής Ενέργειας στην Ελλάδα, σου προσφέρει Ηλεκτρικό
                    Ρεύμα με έκπτωση 45%, επιβραβεύοντάς σε για τη συνέπειά σου
                    και χωρίς καμία χρονική δέσμευση. Με την εμπρόθεσμη εξόφληση
                    του λογαριασμού σου, η έκπτωση αποδίδεται στον επόμενο
                    λογαριασμό. Πάρε κι εσύ τη δύναμη του λογαριασμού σου, στα
                    χέρια σου!
                  </p>
                  <br />
                  <h3>
                    <strong>ΤΙ ΠΛΗΡΩΝΩ</strong>
                  </h3>
                  <br />
                  <table>
                    <thead>
                      <tr className="table-row">
                        <th>ΠΑΓΙΟ (€/ΜΗΝΑ) </th>
                        <th>
                          Χρέωση Ηλεκτρικής Ενέργειας ημέρας με έκπτωση 45%
                          (€/kWh)
                        </th>
                        <th>
                          Χρέωση Ηλεκτρικής Ενέργειας νύχτας με έκπτωση
                          45%(€/kWh)
                        </th>
                        <th>
                          Χρέωση Ηλεκτρικής Ενέργειας ημέρας χωρίς έκπτωση
                          (€/kWh)
                        </th>
                        <th>
                          Χρέωση Ηλεκτρικής Ενέργειας νύχτας χωρίς έκπτωση
                          (€/kWh)
                        </th>
                      </tr>
                      
                    </thead>
                    <tbody>
                      <tr>

                        <td>2,90</td>
                        <td>0,06050</td>
                        <td>0,04317 </td>
                        <td>0,11000</td>
                        <td>0,07850 </td>
                      </tr>
                    </tbody>
                  </table>
                  <br />
                  <p>
                   
                  </p>
                  <br />
                  <h3>
                    <strong>ΕΠΙΠΛΕΟΝ ΠΡΟΝΟΜΙΑ</strong>
                  </h3>

                  <ul className="list-inside" style={{listStyle: 'disc',
    margin: 'revert',
    padding: 'revert'}}>
                    <li>
                      <strong>Μηδενική εγγύηση/προκαταβολή</strong> μέσω της ενεργοποίησης της
                      πάγιας εντολής.
                    </li>
                    <li>
                      <strong>20€ Δώρο</strong> για κάθε χρόνο παραμονής στο πρόγραμμα. Η λήψη
                      του δώρου πραγματοποιείται τον 13ο μήνα εκπροσώπησης.
                    </li>
                    <li>
                      <strong>Επιστροφή 25%</strong>, ως επιβράβευση της καλής και σταθερής
                      συνεργασίας, επί του συνολικού ποσού της εγγύησης, με τη
                      συμπλήρωση κάθε ενός (1) πλήρους έτους συνεργασίας, εφόσον
                      δεν υπάρχουν ληξιπρόθεσμες οφειλές.
                    </li>
                  </ul>
                </div>
                <div className={openTab === 2 ? "block" : "hidden"} id="link2">
                  <h3>
                    <strong>ΧΑΡΑΚΤΗΡΙΣΤΙΚΑ</strong>
                  </h3>
                  <p>
                  Ο ΗΡΩΝ, σου προσφέρει ρεύμα με Νυχτερινή Χρέωση όλο το
                    24ωρο, χωρίς καμία χρονική δέσμευση και χωρίς να απαιτείται
                    η εγκατάσταση νυχτερινού μετρητή. Ακόμα, μπορείς να
                    απολαύσεις σταθερή τιμή χρέωσης ενέργειας ανεξαρτήτως του
                    ύψους της κατανάλωσής σου. Πάρε κι εσύ τη δύναμη της
                    ενέργειας στα χέρια σου!
                    
                  </p>
                  <br />
                  <h3>
                    <strong>ΤΙ ΠΛΗΡΩΝΩ</strong>
                  </h3>
                  <br />
                  <table className="table">
                    <thead>
                      <tr className="table-row">
                        <th>ΠΑΓΙΟ (€/ΜΗΝΑ) </th>
                        <th>
                          Χρέωση Ηλεκτρικής Ενέργειας
                          (€/kWh)
                        </th>
                        
                      </tr>
                    </thead>
                    <tbody>
                      <tr className="table-row">
                      <td>1,90 </td>
                      <td>0,07850</td>
                      </tr>
                    </tbody>
                  </table>
                  <br />
                  <p>
                    
                  </p>
                  <br />
                  <h3>
                    <strong>ΕΠΙΠΛΕΟΝ ΠΡΟΝΟΜΙΑ</strong>
                  </h3>

                  <ul className="list-inside" style={{listStyle: 'disc',
    margin: 'revert',
    padding: 'revert'}}>
                    <li>
                      <strong>20€ Δώρο</strong> για κάθε χρόνο παραμονής στο πρόγραμμα. Η λήψη
                      του δώρου πραγματοποιείται τον 13ο μήνα εκπροσώπησης.
                    </li>
                    <li>
                      <strong>Μηδενική εγγύηση/προκαταβολή</strong> μέσω της ενεργοποίησης της
                      πάγιας εντολής.
                    </li>
                    <li>
                      <strong>Επιστροφή 25%</strong>, ως επιβράβευση της καλής και σταθερής
                      συνεργασίας, επί του συνολικού ποσού της εγγύησης, με τη
                      συμπλήρωση κάθε ενός (1) πλήρους έτους συνεργασίας, εφόσον
                      δεν υπάρχουν ληξιπρόθεσμες οφειλές.
                    </li>
                  </ul>
                </div>
                <div className={openTab === 3 ? "block" : "hidden"} id="link3">
                  <h3>
                    <strong>ΧΑΡΑΚΤΗΡΙΣΤΙΚΑ</strong>
                  </h3>
                  <p>
                    Τώρα έχεις όλη την ενέργεια που χρειάζεσαι, χωρίς χρονική
                    δέσμευση και πάγιο!
                  </p>
                  <br />
                  <h3>
                    <strong>ΤΙ ΠΛΗΡΩΝΩ</strong>
                  </h3>
                  <br />
                  <table>
                    <thead>
                      <tr>
                        <th>ΚΑΤΑΝΑΛΩΣΗ ΗΛΕΚΤΡΙΚΗΣ ΕΝΕΡΓΕΙΑΣ (KWH) </th>
                        <th>ΧΡΕΩΣΗ ΗΛΕΚΤΡΙΚΗΣ ΕΝΕΡΓΕΙΑΣ (€/KWH)</th>
                        <th>ΠΑΓΙΟ (€/ΜΗΝΑ) </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>{"0-2.000 kWh > 2.001 kWh "}</td>
                        <td>0,085 0,092 </td>
                        <td>0 </td>
                      </tr>
                    </tbody>
                  </table>
                  <br />
                  <p>
                    
                  </p>
                  <br />
                  <h3>
                    <strong>ΕΠΙΠΛΕΟΝ ΠΡΟΝΟΜΙΑ</strong>
                  </h3>

                  <ul className="list-inside" style={{listStyle: 'disc',
    margin: 'revert',
    padding: 'revert'}}>
                    <li>
                      <strong>Επιπλέον έκπτωση 1%</strong>, ως επιβράβευση αυτόματης εξόφλησης
                      λογαριασμών μέσω ενεργοποίησης πάγιας εντολής τραπέζης ή
                      πιστωτικής κάρτας.
                    </li>
                    <li>
                      <strong>Μηδενική εγγύηση/προκαταβολή</strong> μέσω της ενεργοποίησης της
                      πάγιας εντολής.
                    </li>
                    <li>
                      <strong>Επιστροφή 25%</strong>, ως επιβράβευση της καλής και σταθερής
                      συνεργασίας, επί του συνολικού ποσού της εγγύησης, με τη
                      συμπλήρωση κάθε ενός (1) πλήρους έτους συνεργασίας, εφόσον
                      δεν υπάρχουν ληξιπρόθεσμες οφειλές.
                    </li>
                  </ul>
                </div>
                <div className={openTab === 4 ? "block" : "hidden"} id="link1">
                  <h3>
                    <strong>ΧΑΡΑΚΤΗΡΙΣΤΙΚΑ</strong>
                  </h3>
                  <p>
                    Και τη Νύχτα, έχεις όλη την ενέργεια που χρειάζεσαι, χωρίς
                    χρονική δέσμευση και πάγιο. Απαιτείται απλά, η εγκατάσταση
                    νυχτερινού μετρητή.
                  </p>
                  <br />
                  <h3>
                    <strong>ΤΙ ΠΛΗΡΩΝΩ</strong>
                  </h3>
                  <br />
                  <table>
                    <thead>
                      <tr>
                        <th>ΚΑΤΑΝΑΛΩΣΗ ΗΛΕΚΤΡΙΚΗΣ ΕΝΕΡΓΕΙΑΣ (KWH) </th>
                        <th>ΧΡΕΩΣΗ ΗΛΕΚΤΡΙΚΗΣ ΕΝΕΡΓΕΙΑΣ ΗΜΕΡΑΣ (€/KWH)</th>
                        <th>ΧΡΕΩΣΗ ΗΛΕΚΤΡΙΚΗΣ ΕΝΕΡΓΕΙΑΣ ΝΥΧΤΑΣ (€/KWH)</th>
                        <th>ΠΑΓΙΟ (€/ΜΗΝΑ)</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>{"0-2.000 kWh > 2.001 kWh "}</td>
                        <td>0,085 0,092 </td>
                        <td>0,0661 0,0661 </td>
                        <td>0 </td>
                      </tr>
                    </tbody>
                  </table>
                  <br />
                  <p>
                    
                  </p>
                  <br />
                  <h3>
                    <strong>ΕΠΙΠΛΕΟΝ ΠΡΟΝΟΜΙΑ</strong>
                  </h3>

                  <ul className="list-inside" style={{listStyle: 'disc',
    margin: 'revert',
    padding: 'revert'}}>
                    <li>
                      <strong>Επιπλέον έκπτωση 1%</strong>, ως επιβράβευση αυτόματης εξόφλησης
                      λογαριασμών μέσω ενεργοποίησης πάγιας εντολής τραπέζης ή
                      πιστωτικής κάρτας.
                    </li>
                    <li>
                      <strong>Μηδενική εγγύηση/προκαταβολή</strong> μέσω της ενεργοποίησης της
                      πάγιας εντολής.
                    </li>
                    <li>
                    <strong>Επιστροφή 25%</strong>, ως επιβράβευση της καλής και σταθερής
                      συνεργασίας, επί του συνολικού ποσού της εγγύησης, με τη
                      συμπλήρωση κάθε ενός (1) πλήρους έτους συνεργασίας, εφόσον
                      δεν υπάρχουν ληξιπρόθεσμες οφειλές.
                    </li>
                  </ul>
                </div>
                <div className={openTab === 5 ? "block" : "hidden"} id="link2">
                  <h3>
                    <strong>ΧΑΡΑΚΤΗΡΙΣΤΙΚΑ</strong>
                  </h3>
                  <p>
                    Με το DOUBLE FIX ECONOMY, εξασφαλίζεις χαμηλή και σταθερή
                    τιμή Φυσικού Αερίου, για είκοσι τέσσερις (24) μήνες. Και
                    επιπλέον δώρο το 45% της μηνιαίας κατανάλωσης Φυσικού Αερίου
                    για δύο χρόνια! Απευθύνεται σε οικιακούς καταναλωτές Φυσικού
                    Αερίου για αυτόνομες οικιακές παροχές που διαθέτουν
                    ταυτόχρονα νέα ή ενεργή σύμβαση προμήθειας Ηλεκτρικής
                    Ενέργειας με τον ΗΡΩΝΑ.
                  </p>
                  <br />
                  <h3>
                    <strong>ΤΙ ΠΛΗΡΩΝΩ</strong>
                  </h3>
                  <br />
                  <table>
                    <thead>
                      <tr>
                        <th>
                          ΣΤΑΘΕΡΗ ΧΡΕΩΣΗ ΠΡΟΜΗΘΕΙΑΣ ΦΥΣΙΚΟΥ ΑΕΡΙΟΥ (€/KWH)
                        </th>
                        <th>ΔΩΡΟ</th>
                        <th>EXTRA ΕΚΠΤΩΣΗ ΣΥΝΕΠΕΙΑΣ*</th>
                        <th>ΠΑΓΙΟ (€/ΜΗΝΑ)</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>0,0165</td>
                        <td>25% της κατανάλωσης Φυσικού Αερίου κάθε μήνα</td>
                        <td>+20% Δωρεάν Ποσότητα κάθε μήνα</td>
                        <td>3,4</td>
                      </tr>
                    </tbody>
                  </table>
                  <br />
                  <p>
                    
                  </p>
                  <br />
                  <h3>
                    <strong>ΕΠΙΠΛΕΟΝ ΠΡΟΝΟΜΙΑ</strong>
                  </h3>

                  <ul className="list-inside" style={{listStyle: 'disc',
    margin: 'revert',
    padding: 'revert'}}>
                    <li>
                      <strong>Δώρο το πάγιο</strong> των καλοκαιρινών μηνών Ιούνιο, Ιούλιο και
                      Αύγουστο.
                    </li>
                    <li>
                      <strong>Μηδενική εγγύηση/προκαταβολή</strong> μέσω της ενεργοποίησης της
                      πάγιας εντολής.
                    </li>
                  </ul>
                </div>
                <div className={openTab === 6 ? "block" : "hidden"} id="link3">
                  <h3>
                    <strong>ΧΑΡΑΚΤΗΡΙΣΤΙΚΑ</strong>
                  </h3>
                  <p>
                    Το Πρόγραμμα DOUBLE FLEX ECONOMY, προσφέρει χαμηλή και
                    σταθερή τιμή Φυσικού Αερίου ανά τρίμηνο, για είκοσι τέσσερις
                    (24) μήνες. Επιπλέον, εξασφαλίζεις, δωρεάν, το 45% της
                    μηνιαίας κατανάλωσης Φυσικού Αερίου για δύο χρόνια!
                    Απευθύνεται σε οικιακούς καταναλωτές Φυσικού Αερίου για
                    αυτόνομες οικιακές παροχές που διαθέτουν ταυτόχρονα νέα ή
                    ενεργή σύμβαση προμήθειας Ηλεκτρικής Ενέργειας με τον ΗΡΩΝΑ.
                  </p>
                  <br />
                  <h3>
                    <strong>ΤΙ ΠΛΗΡΩΝΩ</strong>
                  </h3>
                  <br />
                  <table>
                    <thead>
                      <tr>
                        <th>
                          ΑΝΑΛΟΓΙΚΗ ΧΡΕΩΣΗ ΠΡΟΜΗΘΕΙΑΣ ΦΥΣΙΚΟΥ ΑΕΡΙΟΥ (€/KWH)
                        </th>
                        <th>ΔΩΡΟ</th>
                        <th>EXTRA ΕΚΠΤΩΣΗ ΣΥΝΕΠΕΙΑΣ*</th>
                        <th>ΠΑΓΙΟ (€/ΜΗΝΑ)</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>
                          (Οριστική Μοναδιαία Τιµή Εκκίνησης Εκάστοτε
                          Τριµηνιαίας Δηµοπρασίας + 0,006) * 0.55
                        </td>
                        <td>25% της κατανάλωσης Φυσικού Αερίου κάθε μήνα</td>
                        <td>+20% Δωρεάν Ποσότητα κάθε μήνα</td>
                        <td>3,4</td>
                      </tr>
                    </tbody>
                  </table>
                  <br />
                  <p>
                   
                  </p>
                  <br />
                  <h3>
                    <strong>ΕΠΙΠΛΕΟΝ ΠΡΟΝΟΜΙΑ</strong>
                  </h3>

                  <ul className="list-inside" style={{listStyle: 'disc',
    margin: 'revert',
    padding: 'revert'}}>
                    <li>
                      <strong>Δώρο το πάγιο των καλοκαιρινών μηνών Ιούνιο</strong>, Ιούλιο και
                      Αύγουστο.
                    </li>
                    <li>
                    <strong>Μηδενική εγγύηση/προκαταβολή</strong> μέσω της ενεργοποίησης της
                      πάγιας εντολής.
                    </li>
                  </ul>
                </div>
                <div className={openTab === 7 ? "block" : "hidden"} id="link3">
                  <h3>
                    <strong>ΧΑΡΑΚΤΗΡΙΣΤΙΚΑ</strong>
                  </h3>
                  <p>
                    Ο ΗΡΩΝ, ο κορυφαίος ιδιώτης παραγωγός και προμηθευτής
                    Ηλεκτρικής Ενέργειας στην Ελλάδα, σου προσφέρει Ηλεκτρικό
                    Ρεύμα με έκπτωση έως και 50%, επί της νυχτερινής σου
                    κατανάλωσης, για το ηλεκτρικό σου αυτοκίνητο επιβραβεύοντας
                    τη συνέπειά σου με πρόσθετη έκπτωση 45%. Χρησιμοποιώντας
                    ηλεκτρικό αυτοκίνητο αφήνεις το δικό σου «πράσινο»
                    αποτύπωμα, με τον ΗΡΩΝΑ να συμμετέχει σε αυτό προσφέροντας
                    τα προγράμματα EcoDrive HOME και EcoDrive HOME CHARGE με τα
                    εξής πρόσθετα χαρακτηριστικά:
                  </p><br/>
                  <ul className="list-inside" style={{listStyle: 'disc',
    margin: 'revert',
    padding: 'revert'}}>
                    <li>
                      Ηλεκτρονική λήψη του Λογαριασμού σου για εξοικονόμηση
                      χαρτιού
                    </li>
                    <li>
                      Πράσινο Πιστοποιητικό με το οποίο θα βεβαιώνεται, ότι η
                      ηλεκτρική ενέργεια που κατανάλωσες έχει παραχθεί από
                      Ανανεώσιμες Πηγές Ενέργειας. Κάθε χρόνο ο ΗΡΩΝ θα
                      αναλαμβάνει το κόστος αγοράς και φύτευσης ενός δένδρου για
                      σένα στα πλαίσια προγράμματος δενδροφυτεύσεων.
                    </li>
                  </ul>
                  <p>
                    Επιπλέον, εάν επιθυμείς τη φόρτιση του αυτοκινήτου στο σπίτι
                    σου, με το πρόγραμμα EcoDrive HOME CHARGE ο ΗΡΩΝ αναλαμβάνει
                    να σε προμηθεύσει με φορτιστή τύπου EVBox Elvi, ονομαστικής
                    αξίας 850€ πλέον ΦΠΑ 24%, έναντι μηνιαίας χρέωσης ίσης με το
                    ποσό των 35,42€ πλέον ΦΠΑ 24%.
                  </p>
                  <br />
                  <h3>
                    <strong>ΤΙ ΠΛΗΡΩΝΩ</strong>
                  </h3>
                  <br />
                  <table>
                    <thead>
                      <tr>
                        <th>ΠΑΓΙΟ (€/ΜΗΝΑ)</th>
                        <th>
                          ΧΡΕΩΣΗ ΗΛΕΚΤΡΙΚΗΣ ΕΝΕΡΓΕΙΑΣ ΗΜΕΡΑΣ ΜΕ ΕΚΠΤΩΣΗ
                          ΣΥΝΕΠΕΙΑΣ 45% (€/KWH)
                        </th>
                        
                        <th>
                          ΧΡΕΩΣΗ ΗΛΕΚΤΡΙΚΗΣ ΕΝΕΡΓΕΙΑΣ ΝΥΧΤΑΣ ΜΕΤΑ ΔΩΡΟΥ ΚΑΙ ΜΕ
                          ΠΡΟΣΘΕΤΗ ΕΚΠΤΩΣΗ ΣΥΝΕΠΕΙΑΣ 45% (€/KWH){" "}
                        </th>
                        <th>ΔΩΡΟ</th>
                        <th>
                          ΧΡΕΩΣΗ ΗΛΕΚΤΡΙΚΗΣ ΕΝΕΡΓΕΙΑΣ ΝΥΧΤΑΣ ΧΩΡΙΣ ΕΚΠΤΩΣΗ
                          ΣΥΝΕΠΕΙΑΣ (€/KWH)
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>2,90</td>
                        <td>0,06050</td>
                        
                        <td>0,02159</td>
                        <td>
                          Για μετρητές διπλής εγγραφής: 50% της Κατανάλωσης
                          Ηλεκτρικής Ενέργειας νύχτας*{" "}
                        </td>
                        <td>0,07850</td>
                      </tr>
                    </tbody>
                  </table>
                  <p>
                    
                  </p>
                  <br />
                  <p></p>
                  <br />
                  <h3>
                    <strong>ΕΠΙΠΛΕΟΝ ΠΡΟΝΟΜΙΑ</strong>
                  </h3>

                  <ul className="list-inside" style={{listStyle: 'disc',
    margin: 'revert',
    padding: 'revert'}}  >
                    <li>
                      Οι Εκπτώσεις Συνέπειας του ως άνω Πίνακα αφορούν
                      αποκλειστικά στις ανταγωνιστικές χρεώσεις ημερήσιας και
                      νυχτερινής κατανάλωσης του Λογαριασμού (εξαιρουμένης της
                      Χρέωσης Παγίου).
                    </li>
                    <li>
                      Οι τιμές που αναγράφονται στον ανωτέρω πίνακα αφορούν
                      αποκλειστικά στις ανταγωνιστικές χρεώσεις για την
                      προμήθεια ηλεκτρικής ενέργειας υπό την προϋπόθεση μη
                      ενεργοποίησης της ρήτρας αναπροσαρμογής, κατά την
                      διακριτική ευχέρεια του Προμηθευτή και δεν περιλαμβάνουν
                      τις Ρυθμιζόμενες Χρεώσεις και τον ΦΠΑ. Ακριβής
                      προσδιορισμός όλων των χρεώσεων περιλαμβάνεται λεπτομερώς
                      στους Ειδικούς & Γενικούς Όρους των Προγραμμάτων EcoDrive
                      HOME & EcoDrive HOME CHARGE.
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default HomeTabs;
