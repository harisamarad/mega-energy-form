import { useState, useEffect } from "react";
import Router, { useRouter } from "next/router";

import Button from "../button";

export default function EntryForm() {
  const [_name, setName] = useState("");
  const [_email, setEmail] = useState("");
  const [_phone, setPhone] = useState("");
  const [_contacted, setContacted] = useState('');
  const [submitting, setSubmitting] = useState(false);
  const [disabled, setDisabled] = useState(true);

  const router = useRouter();
  const { id, name, email, phone, contacted } = router.query;

  useEffect(() => {
    if (typeof name === "string") {
      setName(name);
    }
    if (typeof email === "string") {
      setEmail(email);
    }
    if (typeof phone === "number") {
      setPhone(phone);
    }
    if (typeof contacted === "string") {
      setContacted(contacted);
    }
  }, [name, email, phone, contacted]);

  async function submitHandler(e) {
    e.preventDefault();
    setSubmitting(true);
    try {
      const res = await fetch("/api/edit-entry", {
        method: "PATCH",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          id,
          name: _name,
          email: _email,
          phone: _phone,
          contacted: _contacted,
        }),
      });
      const json = await res.json();
      setSubmitting(false);
      if (!res.ok) throw Error(json.message);
      Router.push("/administration");
    } catch (e) {
      throw Error(e.message);
    }
  }
  return (
    <form onSubmit={submitHandler}>
      <div className="my-4">
        <label htmlFor="name">
          <h3 className="font-bold">Name</h3>
        </label>
        <input
          id="name"
          className="shadow border rounded w-full"
          type="text"
          name="name"
          value={_name}
          onChange={(e) => setName(e.target.value)}
        />
      </div>
      <div className="my-4">
        <label htmlFor="email">
          <h3 className="font-bold">Email</h3>
        </label>
        <input
          id="email"
          className="shadow border rounded w-full"
          type="email"
          name="email"
          value={_email}
          onChange={(e) => setEmail(e.target.value)}
        />
      </div>
      <div className="my-4">
        <label htmlFor="phone">
          <h3 className="font-bold">Phone</h3>
        </label>
        <input
          id="phone"
          className="shadow border rounded w-full"
          type="tel"
          name="phone"
          value={_phone}
          onChange={(e) => setPhone(e.target.value)}
        />
      </div>
      <div className="my-4">

        <label htmlFor="contacted">
          <h3 className="font-bold">Contacted</h3>
        </label>
        <input
          id="contacted"
          className="shadow border rounded w-full"
          type="text"
          name="contacted"

          value={_contacted}
          onChange={(e) => setContacted(e.target.value)}
        />
      </div>
      <Button disabled={submitting} type="submit">
        {submitting ? "Saving ..." : "Save"}
      </Button>
    </form>
  );
}
